import React, { useState } from 'react'

const Textarea = (props) => {
    const [Text,setText]=useState('')

    const click=()=>{
        let newtext= Text.toUpperCase()
        setText(newtext)
    }
    const click1=()=>{
        let newtext= Text.toLowerCase()
        setText(newtext)
    }
    const clear=()=>{
      let newtext= ""
      setText(newtext)
  }
    const Change=(e)=>{
        setText(e.target.value)
    }
   
  return (
    <div className='container'>

<div class="mb-3" style={{color:props.mode==='dark'?'white':'black'}}>
  <label for="exampleFormControlTextarea1" class="form-label">Textarea</label>
  <textarea class="form-control" value={Text} id="mybox" rows="8" onChange={Change} style={{backgroundColor:props.mode==='dark'?'grey':'white',
    color:props.mode==='dark'?'white':'black'}}></textarea>
  <button className="btn btn-primary my-2 " onClick={click}>Uppercase</button>
  <button className="btn btn-primary mx-2" onClick={click1}>Lowercase</button>
  <button className="btn btn-primary mx-1" onClick={clear}>Clear</button>


</div>
<div className="container">
  <h1>Text Summary</h1>
  <p>{Text.split(" ").length} Words and {Text.length} Characters</p>
</div>
    </div>
  )
}

export default Textarea
