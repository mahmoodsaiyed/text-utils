import { useState } from 'react';
import './App.css';
import About from './components/About';
import Navbar from './components/Navbar';
import Textarea from './components/Textarea';
import {
  BrowserRouter as Router,
Routes,  Route,
  
} from "react-router-dom";


function App() {
  const[mode,setMode]=useState('light')

  const Dm=()=>{
  if(mode==='light'){
    setMode('dark')
    document.body.style.backgroundColor="grey"
  }
  else{
    setMode('light')
    document.body.style.backgroundColor="white"

  }}
  return (
   <div>

    <Router>
    <Navbar title='TextUtils' abtText='About' mode={mode} Dm={Dm}/>

     <Routes>

          <Route path="/about" element={<About />}>
            
          </Route>
       
          <Route path="/" element={<Textarea mode={mode}/>}>
          

          </Route>
          </Routes>        </Router>

   

</div>

  )
}

export default App;
